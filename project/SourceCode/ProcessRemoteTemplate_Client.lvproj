﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="14008000">
	<Property Name="CCSymbols" Type="Str"></Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Property Name="utf.calculate.project.code.coverage" Type="Bool">false</Property>
	<Property Name="utf.create.arraybrackets" Type="Str">[]</Property>
	<Property Name="utf.create.arraythreshold" Type="UInt">100</Property>
	<Property Name="utf.create.captureinputvalues" Type="Bool">true</Property>
	<Property Name="utf.create.captureoutputvalues" Type="Bool">true</Property>
	<Property Name="utf.create.codecoverage.flag" Type="Bool">false</Property>
	<Property Name="utf.create.codecoverage.value" Type="UInt">100</Property>
	<Property Name="utf.create.editor.flag" Type="Bool">false</Property>
	<Property Name="utf.create.editor.path" Type="Path"></Property>
	<Property Name="utf.create.nameseparator" Type="Str">/</Property>
	<Property Name="utf.create.precision" Type="UInt">6</Property>
	<Property Name="utf.create.repetitions" Type="UInt">1</Property>
	<Property Name="utf.create.testpath.flag" Type="Bool">false</Property>
	<Property Name="utf.create.testpath.path" Type="Path"></Property>
	<Property Name="utf.create.timeout.flag" Type="Bool">false</Property>
	<Property Name="utf.create.timeout.value" Type="UInt">0</Property>
	<Property Name="utf.create.type" Type="UInt">0</Property>
	<Property Name="utf.enable.RT.VI.server" Type="Bool">false</Property>
	<Property Name="utf.passwords" Type="Bin">%1#!#!!!!!)!%%!Q`````Q:4&gt;(*J&lt;G=!!":!1!!"`````Q!!#6"B=X.X&lt;X*E=Q!"!!%!!!!!!!!!!!</Property>
	<Property Name="utf.report.atml.create" Type="Bool">false</Property>
	<Property Name="utf.report.atml.path" Type="Path">ATML report.xml</Property>
	<Property Name="utf.report.atml.view" Type="Bool">false</Property>
	<Property Name="utf.report.details.errors" Type="Bool">false</Property>
	<Property Name="utf.report.details.failed" Type="Bool">false</Property>
	<Property Name="utf.report.details.passed" Type="Bool">false</Property>
	<Property Name="utf.report.errors" Type="Bool">true</Property>
	<Property Name="utf.report.failed" Type="Bool">true</Property>
	<Property Name="utf.report.html.create" Type="Bool">false</Property>
	<Property Name="utf.report.html.path" Type="Path">HTML report.html</Property>
	<Property Name="utf.report.html.view" Type="Bool">false</Property>
	<Property Name="utf.report.passed" Type="Bool">true</Property>
	<Property Name="utf.report.skipped" Type="Bool">true</Property>
	<Property Name="utf.report.sortby" Type="UInt">1</Property>
	<Property Name="utf.report.stylesheet.flag" Type="Bool">false</Property>
	<Property Name="utf.report.stylesheet.path" Type="Path"></Property>
	<Property Name="utf.report.summary" Type="Bool">true</Property>
	<Property Name="utf.report.txt.create" Type="Bool">false</Property>
	<Property Name="utf.report.txt.path" Type="Path">ASCII report.txt</Property>
	<Property Name="utf.report.txt.view" Type="Bool">false</Property>
	<Property Name="utf.run.changed.days" Type="UInt">1</Property>
	<Property Name="utf.run.changed.outdated" Type="Bool">false</Property>
	<Property Name="utf.run.changed.timestamp" Type="Bin">%1#!#!!!!!%!%%"5!!9*2'&amp;U:3^U;7VF!!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	<Property Name="utf.run.days.flag" Type="Bool">false</Property>
	<Property Name="utf.run.includevicallers" Type="Bool">false</Property>
	<Property Name="utf.run.logfile.flag" Type="Bool">false</Property>
	<Property Name="utf.run.logfile.overwrite" Type="Bool">false</Property>
	<Property Name="utf.run.logfile.path" Type="Path">test execution log.txt</Property>
	<Property Name="utf.run.modified.last.run.flag" Type="Bool">true</Property>
	<Property Name="utf.run.priority.flag" Type="Bool">false</Property>
	<Property Name="utf.run.priority.value" Type="UInt">5</Property>
	<Property Name="utf.run.statusfile.flag" Type="Bool">false</Property>
	<Property Name="utf.run.statusfile.path" Type="Path">test status log.txt</Property>
	<Property Name="utf.run.timestamp.flag" Type="Bool">false</Property>
	<Property Name="varPersistentID:{060D4E89-A376-466D-B4B5-EEB78E670366}" Type="Ref">/My Computer/VariablesSequence/Criterias.lvlib/PowerCriteria_101</Property>
	<Property Name="varPersistentID:{07AE6D3B-2471-42E2-AF2F-8C59F28920E6}" Type="Ref">/My Computer/InputVariables/AnalogInInstr.lvlib/PowerDut_102</Property>
	<Property Name="varPersistentID:{0A3DE6BC-32CE-4A3E-A08D-9C8C31C2A47B}" Type="Ref">/My Computer/VariablesSequence/Criterias.lvlib/PressureCriteria_102</Property>
	<Property Name="varPersistentID:{0A90EE42-A828-407D-B308-72091251D9C4}" Type="Ref">/My Computer/VariablesSequence/UserOutputs.lvlib/TimeStable_101</Property>
	<Property Name="varPersistentID:{0BA41040-AF92-4DE6-8FCA-0277E4A0517E}" Type="Ref">/My Computer/InputVariables/AnalogInDuts.lvlib/DUT_106</Property>
	<Property Name="varPersistentID:{0C525168-9511-4A51-B131-7784B450CF41}" Type="Ref">/My Computer/VariablesSequence/RegulatorParameters.lvlib/RegulatorMode_103</Property>
	<Property Name="varPersistentID:{0C6BDBE6-D321-49B1-B115-08AB96D811A5}" Type="Ref">/My Computer/OutputVariables/AnalogOutInstr.lvlib/PowerSupplySp_103</Property>
	<Property Name="varPersistentID:{0FDCE410-34E3-41DC-B110-4AE9F50E4172}" Type="Ref">/My Computer/InputVariables/AnalogInDuts.lvlib/DUT_Raw_103</Property>
	<Property Name="varPersistentID:{17733832-B8CB-445F-BC64-FA9E64BEF6D9}" Type="Ref">/My Computer/InputVariables/AnalogInDuts.lvlib/DUT_102</Property>
	<Property Name="varPersistentID:{2120BA8A-1EF0-4B6D-8BB3-B82BDD563B6D}" Type="Ref">/My Computer/OutputVariables/AnalogOutSbRIO.lvlib/PumpFlowIn_101</Property>
	<Property Name="varPersistentID:{23DA9F67-D75A-4DED-A5D0-04BD62A1D635}" Type="Ref">/My Computer/InputVariables/AnalogInDuts.lvlib/DUT_Raw_105</Property>
	<Property Name="varPersistentID:{24CC1AC5-DB9C-4E46-AC6C-58B277A46FDB}" Type="Ref">/My Computer/VariablesSequence/SupportVariables.lvlib/ZZWatchdogPet_101</Property>
	<Property Name="varPersistentID:{260BDF41-B96B-4C1D-B17F-46948F707D05}" Type="Ref">/My Computer/VariablesSequence/UserOutputs.lvlib/PowerTolerance_101</Property>
	<Property Name="varPersistentID:{26C6477C-68C6-48EA-B7F4-8DDC8D850578}" Type="Ref">/My Computer/VariablesSequence/SequenceData.lvlib/TotalSteps_101</Property>
	<Property Name="varPersistentID:{328FB674-F915-4840-A285-2A29D02180D2}" Type="Ref">/My Computer/InputVariables/AnalogInSbRIO.lvlib/Level_101</Property>
	<Property Name="varPersistentID:{34F2CEBB-CD07-45C4-B1E9-D0EFD9E46145}" Type="Ref">/My Computer/VariablesSequence/UserOutputs.lvlib/PressureTolerance_101</Property>
	<Property Name="varPersistentID:{35833AB8-DC23-470A-BC3A-CF88FFC86B81}" Type="Ref">/My Computer/InputVariables/AnalogInDuts.lvlib/DUT_105</Property>
	<Property Name="varPersistentID:{3D5395A5-3344-4934-A129-3A86021B2FB5}" Type="Ref">/My Computer/InputVariables/AnalogInDuts.lvlib/DUT_Raw_102</Property>
	<Property Name="varPersistentID:{3FA11FE0-D618-4FF0-BBD0-9C963F03ECBA}" Type="Ref">/My Computer/OutputVariables/AnalogOutSbRIO.lvlib/PumpFlowOut_101</Property>
	<Property Name="varPersistentID:{40872741-68DC-468B-9847-9EE3BCC8B89C}" Type="Ref">/My Computer/InputVariables/AnalogInDuts.lvlib/DUT_Raw_108</Property>
	<Property Name="varPersistentID:{41F79381-CE82-460A-B6B2-AF2CF6E32E8E}" Type="Ref">/My Computer/OutputVariables/DigitalOutInstr.lvlib/PowerSupplyOn_101</Property>
	<Property Name="varPersistentID:{42DDAE2C-7C1B-41EA-A526-31EE813E11CF}" Type="Ref">/My Computer/VariablesSequence/RegulatorParameters.lvlib/RegulatorMode_102</Property>
	<Property Name="varPersistentID:{43AD5DA1-1622-4701-A6E6-2FFDF136BE18}" Type="Ref">/My Computer/VariablesSequence/UserOutputs.lvlib/LevelTolerance_101</Property>
	<Property Name="varPersistentID:{443CAE12-151E-4AE7-8F6F-D7BF52229FC5}" Type="Ref">/My Computer/VariablesSequence/RegulatorParameters.lvlib/RegulatorMode_101</Property>
	<Property Name="varPersistentID:{45CC540E-3D5B-46C9-8D1F-8E07E77454A8}" Type="Ref">/My Computer/OutputVariables/DigitalOutCRIO.lvlib/ValveDrain_101</Property>
	<Property Name="varPersistentID:{46C1A16C-9F26-4904-AB07-7E70250C0467}" Type="Ref">/My Computer/InputVariables/AnalogInDuts.lvlib/DUT_Raw_104</Property>
	<Property Name="varPersistentID:{495419D1-85D6-4A00-84B0-A8F45523A64C}" Type="Ref">/My Computer/VariablesSequence/SequenceLogic.lvlib/NextState_101</Property>
	<Property Name="varPersistentID:{53DE13B0-6135-47D2-886C-7F252D51830C}" Type="Ref">/My Computer/VariablesSequence/SequenceData.lvlib/StateTime_101</Property>
	<Property Name="varPersistentID:{578020F3-74DF-4B4C-B762-28832349C4DF}" Type="Ref">/My Computer/OutputVariables/DigitalOutCRIO.lvlib/PumpFlowInOn_101</Property>
	<Property Name="varPersistentID:{597C6DB6-872B-454B-86F8-4287EE205857}" Type="Ref">/My Computer/VariablesSequence/Setpoints.lvlib/SpPower_101</Property>
	<Property Name="varPersistentID:{5A6B1A8A-F27B-45E5-9147-B1F3B05655BF}" Type="Ref">/My Computer/OutputVariables/DigitalOutCRIO.lvlib/PumpFlowOutOn_101</Property>
	<Property Name="varPersistentID:{5F252E36-F936-40DF-BADD-4DF28FCCF4CD}" Type="Ref">/My Computer/OutputVariables/AnalogOutInstr.lvlib/PowerSupplySp_102</Property>
	<Property Name="varPersistentID:{65048C40-2813-451E-A304-02623E9A1401}" Type="Ref">/My Computer/VariablesSequence/UserOutputs.lvlib/DutType_101</Property>
	<Property Name="varPersistentID:{68073E6E-46F0-49E2-9EB7-2CF8CF0F6A38}" Type="Ref">/My Computer/VariablesSequence/Setpoints.lvlib/SpPressure_102</Property>
	<Property Name="varPersistentID:{6CA65C30-F86B-41B1-A289-D4E4F6DC7A51}" Type="Ref">/My Computer/VariablesSequence/Criterias.lvlib/PressureCriteria_101</Property>
	<Property Name="varPersistentID:{744A666A-24F0-4BF6-AC75-E2D17A4A31D9}" Type="Ref">/My Computer/VariablesSequence/SupportVariables.lvlib/StopApp_101</Property>
	<Property Name="varPersistentID:{76A46827-3DBC-49A4-BAEF-13E9F7CE1C7C}" Type="Ref">/My Computer/VariablesSequence/Setpoints.lvlib/SpPressure_101</Property>
	<Property Name="varPersistentID:{7B8A16B0-BAA1-48E4-A361-B1C3CCA7D62F}" Type="Ref">/My Computer/InputVariables/AnalogInSbRIO.lvlib/PressureOut_101</Property>
	<Property Name="varPersistentID:{7BEEE402-48A3-48E5-A212-D416B7A0C0AF}" Type="Ref">/My Computer/VariablesSequence/SequenceLogic.lvlib/Pause_101</Property>
	<Property Name="varPersistentID:{7CCB7905-2318-498E-96E6-521AB4AAE896}" Type="Ref">/My Computer/InputVariables/AnalogInDuts.lvlib/DUT_104</Property>
	<Property Name="varPersistentID:{84FC7F31-4F53-4B50-B046-EB57D2F749F9}" Type="Ref">/My Computer/InputVariables/AnalogInDuts.lvlib/DUT_Raw_101</Property>
	<Property Name="varPersistentID:{870E475A-8264-4E43-964D-86158502202C}" Type="Ref">/My Computer/InputVariables/AnalogInInstr.lvlib/PowerDut_101</Property>
	<Property Name="varPersistentID:{8DF79D92-D24B-4BA0-A408-B8D6E2A2DF86}" Type="Ref">/My Computer/VariablesSequence/Setpoints.lvlib/SpPower_102</Property>
	<Property Name="varPersistentID:{902952E6-AC76-4F6F-9E65-BC4C7E4E16DF}" Type="Ref">/My Computer/VariablesSequence/Criterias.lvlib/TimeStableCriteria_101</Property>
	<Property Name="varPersistentID:{90BAEC24-C22D-4BFD-9035-170769CC4C68}" Type="Ref">/My Computer/VariablesSequence/SequenceLogic.lvlib/Criteria_101</Property>
	<Property Name="varPersistentID:{92A3F152-DAC3-40FA-A40D-B801BA4BB86E}" Type="Ref">/My Computer/OutputVariables/DigitalOutCRIO.lvlib/ValveOutDut_101</Property>
	<Property Name="varPersistentID:{955FE671-50F4-4458-9651-0495E756BD28}" Type="Ref">/My Computer/VariablesSequence/SupportVariables.lvlib/StableTimer_101</Property>
	<Property Name="varPersistentID:{9DF49DC6-6120-43CE-8544-B8C8AD7A42A9}" Type="Ref">/My Computer/VariablesSequence/UserOutputs.lvlib/NumberOfDuts_101</Property>
	<Property Name="varPersistentID:{9F7571D1-9E03-4254-B84A-D784040D1170}" Type="Ref">/My Computer/InputVariables/AnalogInDuts.lvlib/DUT_108</Property>
	<Property Name="varPersistentID:{AF4C23A0-303C-4077-985D-89109D6EEF04}" Type="Ref">/My Computer/VariablesSequence/Criterias.lvlib/CompCriteria_101</Property>
	<Property Name="varPersistentID:{B0DDAF58-9364-4AA4-9B66-71ADA58F8CDF}" Type="Ref">/My Computer/VariablesSequence/SequenceData.lvlib/CurrentStep_101</Property>
	<Property Name="varPersistentID:{B2E936AF-56FB-480E-A7BD-A67C11FAAED8}" Type="Ref">/My Computer/VariablesSequence/SequenceData.lvlib/CurrentState_101</Property>
	<Property Name="varPersistentID:{B3BE74CA-7A88-4272-8F0D-A30BEFF1C872}" Type="Ref">/My Computer/InputVariables/AnalogInDuts.lvlib/DUT_107</Property>
	<Property Name="varPersistentID:{B51CFCBF-091E-451D-96FA-F3FDACC1EEAB}" Type="Ref">/My Computer/VariablesSequence/UserOutputs.lvlib/Timeout_101</Property>
	<Property Name="varPersistentID:{B57CF157-604C-42F7-9050-A369B382A697}" Type="Ref">/My Computer/VariablesSequence/Setpoints.lvlib/SpPower_103</Property>
	<Property Name="varPersistentID:{B7214772-5D47-4036-B619-5180B7C8A8A9}" Type="Ref">/My Computer/VariablesSequence/Criterias.lvlib/LevelCriteria_101</Property>
	<Property Name="varPersistentID:{BEEC772A-B454-40E6-8136-FE2E9C19A0B7}" Type="Ref">/My Computer/OutputVariables/DigitalOutCRIO.lvlib/PumpStepperOn_101</Property>
	<Property Name="varPersistentID:{C040EAB8-9D28-4FE7-869D-341147E5E82F}" Type="Ref">/My Computer/OutputVariables/AnalogOutCRIO.lvlib/PumpStepper_101</Property>
	<Property Name="varPersistentID:{C2D85A59-F0C5-4B01-A6B0-EA7C84276137}" Type="Ref">/My Computer/InputVariables/AnalogInDuts.lvlib/DUT_Raw_106</Property>
	<Property Name="varPersistentID:{C4C65126-BCBC-4CB0-B259-F28512220D0C}" Type="Ref">/My Computer/OutputVariables/AnalogOutInstr.lvlib/PowerSupplySp_101</Property>
	<Property Name="varPersistentID:{CC30CE6A-E4F1-4657-9C46-D85B4A98784A}" Type="Ref">/My Computer/InputVariables/AnalogInSbRIO.lvlib/PressureIn_101</Property>
	<Property Name="varPersistentID:{CEA3B9DC-BD92-4800-90A1-9C5EDA923B42}" Type="Ref">/My Computer/VariablesSequence/SequenceData.lvlib/CurrentJump_101</Property>
	<Property Name="varPersistentID:{D28EC8BD-8B05-463A-820A-98387408470D}" Type="Ref">/My Computer/InputVariables/AnalogInDuts.lvlib/DUT_101</Property>
	<Property Name="varPersistentID:{E23BF7F9-45A8-4C59-AFAE-898E0110E738}" Type="Ref">/My Computer/VariablesSequence/Criterias.lvlib/TimeoutCriteria_101</Property>
	<Property Name="varPersistentID:{E4DBBC03-570B-4766-9F88-E6A2A150EDDF}" Type="Ref">/My Computer/OutputVariables/DigitalOutCRIO.lvlib/ValveInDut_101</Property>
	<Property Name="varPersistentID:{E580808E-35BC-4276-BF98-1AA64D5BCF53}" Type="Ref">/My Computer/VariablesSequence/UserOutputs.lvlib/PressureTolerance_102</Property>
	<Property Name="varPersistentID:{F46AA08D-EA34-4333-A10A-554B6B08F084}" Type="Ref">/My Computer/InputVariables/AnalogInInstr.lvlib/PowerDut_103</Property>
	<Property Name="varPersistentID:{F8C38B7D-E235-40D6-BE6E-4630E8F31FD2}" Type="Ref">/My Computer/InputVariables/AnalogInDuts.lvlib/DUT_103</Property>
	<Property Name="varPersistentID:{FB4039BC-C79D-46A1-82FC-2E2A72D898B4}" Type="Ref">/My Computer/VariablesSequence/Setpoints.lvlib/SpLevel_101</Property>
	<Property Name="varPersistentID:{FD61DFE8-BDDB-4C1A-8B22-D0CF82427362}" Type="Ref">/My Computer/InputVariables/AnalogInDuts.lvlib/DUT_Raw_107</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="CCSymbols" Type="Str"></Property>
		<Property Name="DisableAutoDeployVariables" Type="Bool">true</Property>
		<Property Name="IOScan.Faults" Type="Str">1.0,0;</Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">100000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="mathScriptPath" Type="Str">\\psf\Home\Documents\LabVIEW Data</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.acl" Type="Str">0800000008000000</Property>
		<Property Name="server.tcp.enabled" Type="Bool">true</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str"></Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.access" Type="Str"></Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.viscripting.showScriptingOperationsInContextHelp" Type="Bool">true</Property>
		<Property Name="server.viscripting.showScriptingOperationsInEditor" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="InputVariables" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="AnalogInInstr.lvlib" Type="Library" URL="../Variables/AnalogInInstr.lvlib"/>
			<Item Name="AnalogInDuts.lvlib" Type="Library" URL="../Variables/AnalogInDuts.lvlib"/>
			<Item Name="AnalogInSbRIO.lvlib" Type="Library" URL="../Variables/AnalogInSbRIO.lvlib"/>
		</Item>
		<Item Name="OutputVariables" Type="Folder">
			<Item Name="AnalogOutInstr.lvlib" Type="Library" URL="../Variables/AnalogOutInstr.lvlib"/>
			<Item Name="AnalogOutSbRIO.lvlib" Type="Library" URL="../Variables/AnalogOutSbRIO.lvlib"/>
			<Item Name="AnalogOutCRIO.lvlib" Type="Library" URL="../Variables/AnalogOutCRIO.lvlib"/>
			<Item Name="DigitalOutCRIO.lvlib" Type="Library" URL="../Variables/DigitalOutCRIO.lvlib"/>
			<Item Name="DigitalOutInstr.lvlib" Type="Library" URL="../Variables/DigitalOutInstr.lvlib"/>
		</Item>
		<Item Name="VariablesSequence" Type="Folder">
			<Property Name="NI.SortType" Type="Int">0</Property>
			<Item Name="Criterias.lvlib" Type="Library" URL="../Variables/Criterias.lvlib"/>
			<Item Name="RegulatorParameters.lvlib" Type="Library" URL="../Variables/RegulatorParameters.lvlib"/>
			<Item Name="SequenceData.lvlib" Type="Library" URL="../Variables/SequenceData.lvlib"/>
			<Item Name="SequenceLogic.lvlib" Type="Library" URL="../Variables/SequenceLogic.lvlib"/>
			<Item Name="Setpoints.lvlib" Type="Library" URL="../Variables/Setpoints.lvlib"/>
			<Item Name="SupportVariables.lvlib" Type="Library" URL="../Variables/SupportVariables.lvlib"/>
			<Item Name="UserOutputs.lvlib" Type="Library" URL="../Variables/UserOutputs.lvlib"/>
		</Item>
		<Item Name="Misc" Type="Folder">
			<Property Name="NI.SortType" Type="Int">0</Property>
			<Item Name="AlarmType.ctl" Type="VI" URL="../Misc/AlarmType.ctl"/>
			<Item Name="ReferenceInstruments.ini" Type="Document" URL="../Misc/ReferenceInstruments.ini"/>
		</Item>
		<Item Name="ProjectHMI.lvclass" Type="LVClass" URL="../ProjectHMI/ProjectHMI.lvclass"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Property Name="NI.SortType" Type="Int">0</Property>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Append Waveforms.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Append Waveforms.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="AsciiToInt.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/AsciiToInt.vi"/>
				<Item Name="Bit-array To Byte-array.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/Bit-array To Byte-array.vi"/>
				<Item Name="Boolean Array to Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDT.llb/Boolean Array to Digital.vi"/>
				<Item Name="BuildErrorSource.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/BuildErrorSource.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Built App File Layout.vi" Type="VI" URL="/&lt;vilib&gt;/AppBuilder/Built App File Layout.vi"/>
				<Item Name="Calc Long Word Padded Width.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Calc Long Word Padded Width.vi"/>
				<Item Name="Check Color Table Size.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Color Table Size.vi"/>
				<Item Name="Check Data Size.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Data Size.vi"/>
				<Item Name="Check File Permissions.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check File Permissions.vi"/>
				<Item Name="Check for Equality.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Check for Equality.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Path.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Path.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Clear-68016.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/Clear-68016.vi"/>
				<Item Name="ClearError.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/ClearError.vi"/>
				<Item Name="Close Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Close Registry Key.vi"/>
				<Item Name="Color to RGB.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/colorconv.llb/Color to RGB.vi"/>
				<Item Name="Compare Two Paths.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Compare Two Paths.vi"/>
				<Item Name="compatOverwrite.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOverwrite.vi"/>
				<Item Name="configureNumberOfValues.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/configureNumberOfValues.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Create ActiveX Event Queue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Create ActiveX Event Queue.vi"/>
				<Item Name="Create Directory Recursive.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Create Directory Recursive.vi"/>
				<Item Name="Create Error Clust.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Create Error Clust.vi"/>
				<Item Name="Create Mask By Alpha.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Create Mask By Alpha.vi"/>
				<Item Name="Destroy ActiveX Event Queue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Destroy ActiveX Event Queue.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Digital Size.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDT.llb/Digital Size.vi"/>
				<Item Name="Directory of Top Level VI.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Directory of Top Level VI.vi"/>
				<Item Name="DTbl Boolean Array to Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Boolean Array to Digital.vi"/>
				<Item Name="DTbl Compress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Compress Digital.vi"/>
				<Item Name="DTbl Digital Size.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital Size.vi"/>
				<Item Name="DWDT Boolean Array to Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Boolean Array to Digital.vi"/>
				<Item Name="DWDT Digital Size.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Digital Size.vi"/>
				<Item Name="DWDT Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Error Code.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="Escape Characters for HTTP.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Escape Characters for HTTP.vi"/>
				<Item Name="EventData.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/EventData.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="ExtractSubstring.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/ExtractSubstring.vi"/>
				<Item Name="FileVersionInfo.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FileVersionInfo.vi"/>
				<Item Name="FileVersionInformation.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FileVersionInformation.ctl"/>
				<Item Name="fileViewerConfigData.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/fileViewerConfigData.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="FixedFileInfo_Struct.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FixedFileInfo_Struct.ctl"/>
				<Item Name="Flip and Pad for Picture Control.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Flip and Pad for Picture Control.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="formatPropertyList.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/formatPropertyList.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Generate Temporary File Path.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Generate Temporary File Path.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="Get LV Class Path.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Path.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Get Type of Variant.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/GetType.llb/Get Type of Variant.vi"/>
				<Item Name="getChannelList.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/getChannelList.vi"/>
				<Item Name="GetFileVersionInfo.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/GetFileVersionInfo.vi"/>
				<Item Name="GetFileVersionInfoSize.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/GetFileVersionInfoSize.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="getNamesFromPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/getNamesFromPath.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="GoTo.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/GoTo.vi"/>
				<Item Name="Handle Open Word or Excel File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/NIReport.llb/Toolkit/Handle Open Word or Excel File.vi"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
				<Item Name="initFileContentsTree.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/initFileContentsTree.vi"/>
				<Item Name="InitFromConfiguration.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/InitFromConfiguration.vi"/>
				<Item Name="initHelpButtonVisibility.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/initHelpButtonVisibility.vi"/>
				<Item Name="InitScrollbarAndListBox.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/InitScrollbarAndListBox.vi"/>
				<Item Name="initTabValues.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/initTabValues.vi"/>
				<Item Name="List Directory and LLBs.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/List Directory and LLBs.vi"/>
				<Item Name="loadAndFormatValues.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/loadAndFormatValues.vi"/>
				<Item Name="LoadBufferForMultiListBoxAndFormat.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/LoadBufferForMultiListBoxAndFormat.vi"/>
				<Item Name="LogicalSort.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/LogicalSort.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="LVRowAndColumnUnsignedTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRowAndColumnUnsignedTypeDef.ctl"/>
				<Item Name="MoveMemory.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/MoveMemory.vi"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="NI_AALPro.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALPro.lvlib"/>
				<Item Name="NI_Excel.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/Excel/NI_Excel.lvclass"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_Gmath.lvlib" Type="Library" URL="/&lt;vilib&gt;/gmath/NI_Gmath.lvlib"/>
				<Item Name="NI_HTML.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/HTML/NI_HTML.lvclass"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_Matrix.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/Matrix/NI_Matrix.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_PtbyPt.lvlib" Type="Library" URL="/&lt;vilib&gt;/ptbypt/NI_PtbyPt.lvlib"/>
				<Item Name="NI_report.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/NI_report.lvclass"/>
				<Item Name="NI_ReportGenerationCore.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/NIReport.llb/NI_ReportGenerationCore.lvlib"/>
				<Item Name="NI_ReportGenerationToolkit.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/_office/NI_ReportGenerationToolkit.lvlib"/>
				<Item Name="NI_Standard Report.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/Standard Report/NI_Standard Report.lvclass"/>
				<Item Name="ni_tagger_lv_FlushAllConnections.vi" Type="VI" URL="/&lt;vilib&gt;/variable/tagger/ni_tagger_lv_FlushAllConnections.vi"/>
				<Item Name="NI_VariableUtilities.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Variable/NI_VariableUtilities.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="OccFireType.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/OccFireType.ctl"/>
				<Item Name="Open Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Open Registry Key.vi"/>
				<Item Name="panelResize_tdms.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/panelResize_tdms.vi"/>
				<Item Name="panelstate.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/panelstate.ctl"/>
				<Item Name="Path to URL.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Path to URL.vi"/>
				<Item Name="Read JPEG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Read JPEG File.vi"/>
				<Item Name="Read PNG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/png.llb/Read PNG File.vi"/>
				<Item Name="Read Registry Value DWORD.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value DWORD.vi"/>
				<Item Name="Read Registry Value Simple STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple STR.vi"/>
				<Item Name="Read Registry Value Simple U32.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple U32.vi"/>
				<Item Name="Read Registry Value Simple.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple.vi"/>
				<Item Name="Read Registry Value STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value STR.vi"/>
				<Item Name="Read Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value.vi"/>
				<Item Name="Recursive File List.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Recursive File List.vi"/>
				<Item Name="Registry Handle Master.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Handle Master.vi"/>
				<Item Name="Registry refnum.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry refnum.ctl"/>
				<Item Name="Registry RtKey.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry RtKey.ctl"/>
				<Item Name="Registry SAM.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry SAM.ctl"/>
				<Item Name="Registry Simplify Data Type.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Simplify Data Type.vi"/>
				<Item Name="Registry View.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry View.ctl"/>
				<Item Name="Registry WinErr-LVErr.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry WinErr-LVErr.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Busy.vi"/>
				<Item Name="Set Cursor (Cursor ID).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Cursor ID).vi"/>
				<Item Name="Set Cursor (Icon Pict).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Icon Pict).vi"/>
				<Item Name="Set Cursor.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="setListBoxColumnWidths.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/setListBoxColumnWidths.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="sizeaction.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/sizeaction.ctl"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="status.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/status.vi"/>
				<Item Name="STR_ASCII-Unicode.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/STR_ASCII-Unicode.vi"/>
				<Item Name="subDisplayMessage.vi" Type="VI" URL="/&lt;vilib&gt;/express/express output/DisplayMessageBlock.llb/subDisplayMessage.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="subTimeDelay.vi" Type="VI" URL="/&lt;vilib&gt;/express/express execution control/TimeDelayBlock.llb/subTimeDelay.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="TDMS - File Viewer.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/TDMS - File Viewer.vi"/>
				<Item Name="TDMSFileViewer_LaunchHelp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/TDMSFileViewer_LaunchHelp.vi"/>
				<Item Name="TDMSFileViewerLocalizedText.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/TDMSFileViewerLocalizedText.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Type Enum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/GetType.llb/Type Enum.ctl"/>
				<Item Name="Unset Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Unset Busy.vi"/>
				<Item Name="UpdateBufferForMultiListBoxIfNecessary.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/UpdateBufferForMultiListBoxIfNecessary.vi"/>
				<Item Name="UpdateListBoxAfterKeyEvent.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/UpdateListBoxAfterKeyEvent.vi"/>
				<Item Name="UpdateScrollbarBeforeKeyEvent.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/UpdateScrollbarBeforeKeyEvent.vi"/>
				<Item Name="VariantType to Type Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/GetType.llb/VariantType to Type Code.vi"/>
				<Item Name="VariantType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/VariantDataType/VariantType.lvlib"/>
				<Item Name="VerQueryValue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/VerQueryValue.vi"/>
				<Item Name="Version To Dotted String.vi" Type="VI" URL="/&lt;vilib&gt;/_xctls/Version To Dotted String.vi"/>
				<Item Name="Wait On ActiveX Event.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Wait On ActiveX Event.vi"/>
				<Item Name="Wait types.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Wait types.ctl"/>
				<Item Name="WDT Append Waveforms CDB.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms CDB.vi"/>
				<Item Name="WDT Append Waveforms CXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms CXT.vi"/>
				<Item Name="WDT Append Waveforms DBL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms DBL.vi"/>
				<Item Name="WDT Append Waveforms EXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms EXT.vi"/>
				<Item Name="WDT Append Waveforms I16.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms I16.vi"/>
				<Item Name="WDT Append Waveforms I32.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms I32.vi"/>
				<Item Name="WDT Append Waveforms I64.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms I64.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Write BMP Data To Buffer.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP Data To Buffer.vi"/>
				<Item Name="Write BMP Data.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP Data.vi"/>
				<Item Name="Write BMP File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP File.vi"/>
				<Item Name="Write JPEG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Write JPEG File.vi"/>
				<Item Name="Write PNG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/png.llb/Write PNG File.vi"/>
				<Item Name="XControlSupport.lvlib" Type="Library" URL="/&lt;vilib&gt;/_xctls/XControlSupport.lvlib"/>
			</Item>
			<Item Name="Advapi32.dll" Type="Document" URL="Advapi32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Alarm.lvclass" Type="LVClass" URL="../../../labqt/main/Alarm/Alarm/Alarm.lvclass"/>
			<Item Name="Alarm.xctl" Type="XControl" URL="../../../labqt/main/Alarm/Alarm/XControl/Alarm/Alarm.xctl"/>
			<Item Name="AlarmCodec.lvclass" Type="LVClass" URL="../../../labqt/main/Codecs/AlarmCodec/AlarmCodec.lvclass"/>
			<Item Name="AlarmConfiguration.xctl" Type="XControl" URL="../../../labqt/main/Alarm/Alarm/XControl/AlarmConfigurator/AlarmConfiguration.xctl"/>
			<Item Name="AlarmEvent.lvclass" Type="LVClass" URL="../../../labqt/main/Alarm/AlarmEvent/AlarmEvent.lvclass"/>
			<Item Name="AlarmHandler.lvclass" Type="LVClass" URL="../../../labqt/main/Alarm/AlarmHandler/AlarmHandler.lvclass"/>
			<Item Name="AlarmInitData.ctl" Type="VI" URL="../../../labqt/main/Alarm/Alarm/XControl/AlarmConfigurator/AlarmInitData.ctl"/>
			<Item Name="AlarmLister.xctl" Type="XControl" URL="../../../labqt/main/Alarm/Alarm/XControl/AlarmLister/AlarmLister.xctl"/>
			<Item Name="AlarmObjectAttributes.ctl" Type="VI" URL="../../../labqt/main/Alarm/Alarm/AlarmObjectAttributes.ctl"/>
			<Item Name="AlarmRapi.lvclass" Type="LVClass" URL="../../../labqt/main/RemoteProcedureCall/RemoteApis/AlarmRapi/AlarmRapi.lvclass"/>
			<Item Name="ApplicationBase.lvclass" Type="LVClass" URL="../../../labqt/main/Application/ApplicationBase/ApplicationBase.lvclass"/>
			<Item Name="BroadcastEventType.ctl" Type="VI" URL="../../../labqt/main/GUI/SubPanelUtilities/BroadcastEventType.ctl"/>
			<Item Name="BroadCastIncomingEvent.ctl" Type="VI" URL="../../../labqt/main/GUI/SubPanelUtilities/BroadCastIncomingEvent.ctl"/>
			<Item Name="BusyDialog.vi" Type="VI" URL="../../../labqt/main/Utilities/Dialog/BusyDialog/BusyDialog.vi"/>
			<Item Name="BusyDialogState.ctl" Type="VI" URL="../../../labqt/main/Utilities/Dialog/BusyDialog/BusyDialogState.ctl"/>
			<Item Name="CalculateKandM.vi" Type="VI" URL="../../../labqt/main/Utilities/Calculation/CalculateKandM.vi"/>
			<Item Name="Calibrate.lvclass" Type="LVClass" URL="../../../labqt/main/Calibration/Calibrate/Calibrate.lvclass"/>
			<Item Name="CalibrationConfiguration.xctl" Type="XControl" URL="../../../labqt/main/Calibration/Calibrate/XControl/CalibrationConfiguration/CalibrationConfiguration.xctl"/>
			<Item Name="CalibrationLister.xctl" Type="XControl" URL="../../../labqt/main/Calibration/Calibrate/XControl/CalibrationLister/CalibrationLister.xctl"/>
			<Item Name="CalibrationProcedure.lvclass" Type="LVClass" URL="../../../labqt/main/Calibration/CalibrationProcedure/CalibrationProcedure.lvclass"/>
			<Item Name="ChannelIdDelimiter.ctl" Type="VI" URL="../../../labqt/main/Sequence/SequenceInputRunner/ChannelIdDelimiter.ctl"/>
			<Item Name="Codec.lvclass" Type="LVClass" URL="../../../labqt/main/Codecs/Codec/Codec.lvclass"/>
			<Item Name="CompareFeedbackControlSequence.lvclass" Type="LVClass" URL="../../../labqt/main/Sequence/Concrete/CompareFeedbackControlSequence/CompareFeedbackControlSequence.lvclass"/>
			<Item Name="CompressToMean.vi" Type="VI" URL="../../../labqt/main/Utilities/Waveform/CompressToMean.vi"/>
			<Item Name="Controller.lvclass" Type="LVClass" URL="../../../labqt/main/ControlSystem/Controller/Controller.lvclass"/>
			<Item Name="CRioCSeriesDeviceReader.lvclass" Type="LVClass" URL="../../../labqt/drivers/DAQ/CRioCSeriesDeviceReader/CRioCSeriesDeviceReader.lvclass"/>
			<Item Name="CRioCSeriesDeviceWriter.lvclass" Type="LVClass" URL="../../../labqt/drivers/DAQ/CRioCSeriesDeviceWriter/CRioCSeriesDeviceWriter.lvclass"/>
			<Item Name="DaqCodec.lvclass" Type="LVClass" URL="../../../labqt/main/Codecs/DaqCodec/DaqCodec.lvclass"/>
			<Item Name="DaqRapi.lvclass" Type="LVClass" URL="../../../labqt/main/RemoteProcedureCall/RemoteApis/DaqRapi/DaqRapi.lvclass"/>
			<Item Name="DeviceReader.lvclass" Type="LVClass" URL="../../../labqt/main/IOStreams/DeviceReader/DeviceReader.lvclass"/>
			<Item Name="DeviceWriter.lvclass" Type="LVClass" URL="../../../labqt/main/IOStreams/DeviceWriter/DeviceWriter.lvclass"/>
			<Item Name="DisplayLogWriter.lvclass" Type="LVClass" URL="../../../labqt/main/Logging/DisplayLogWriter/DisplayLogWriter.lvclass"/>
			<Item Name="EasyViewFileWriter.lvclass" Type="LVClass" URL="../../../labqt/main/IOStreams/EasyViewFileWriter/EasyViewFileWriter.lvclass"/>
			<Item Name="Event.lvclass" Type="LVClass" URL="../../../labqt/main/Events/Event/Event.lvclass"/>
			<Item Name="EventHandler.lvclass" Type="LVClass" URL="../../../labqt/main/Events/EventHandler/EventHandler.lvclass"/>
			<Item Name="ExecutionBase.lvclass" Type="LVClass" URL="../../../labqt/main/Execution/ExecutionBase/ExecutionBase.lvclass"/>
			<Item Name="FeedbackControlCodec.lvclass" Type="LVClass" URL="../../../labqt/main/Codecs/FeedbackControlCodec/FeedbackControlCodec.lvclass"/>
			<Item Name="FeedbackController.lvclass" Type="LVClass" URL="../../../labqt/main/ControlSystem/FeedbackController/FeedbackController.lvclass"/>
			<Item Name="FeedbackControllerRapi.lvclass" Type="LVClass" URL="../../../labqt/main/RemoteProcedureCall/RemoteApis/FeedbackControllerRapi/FeedbackControllerRapi.lvclass"/>
			<Item Name="FeedbackControlSequence.lvclass" Type="LVClass" URL="../../../labqt/main/Sequence/Concrete/FeedbackControlSequence/FeedbackControlSequence.lvclass"/>
			<Item Name="FileLogWriter.lvclass" Type="LVClass" URL="../../../labqt/main/Logging/FileLogWriter/FileLogWriter.lvclass"/>
			<Item Name="FileReader.lvclass" Type="LVClass" URL="../../../labqt/main/IOStreams/FileReader/FileReader.lvclass"/>
			<Item Name="FileStream.lvclass" Type="LVClass" URL="../../../labqt/main/IOStreams/FileStream/FileStream.lvclass"/>
			<Item Name="FileWriter.lvclass" Type="LVClass" URL="../../../labqt/main/IOStreams/FileWriter/FileWriter.lvclass"/>
			<Item Name="FloatingPointSequence.lvclass" Type="LVClass" URL="../../../labqt/main/Sequence/FloatingPointSequence/FloatingPointSequence.lvclass"/>
			<Item Name="FloatingPointSequenceRapi.lvclass" Type="LVClass" URL="../../../labqt/main/RemoteProcedureCall/RemoteApis/FloatingpointSequenceRapi/FloatingPointSequenceRapi.lvclass"/>
			<Item Name="FunctionCalibration.lvclass" Type="LVClass" URL="../../../labqt/main/Calibration/FunctionCalibration/FunctionCalibration.lvclass"/>
			<Item Name="GetRawSocketFromConnectionID.vi" Type="VI" URL="../../../labqt/main/IOStreams/SupportVIs/GetRawSocketFromConnectionID.vi"/>
			<Item Name="HtmlFileLogWriter.lvclass" Type="LVClass" URL="../../../labqt/main/Logging/HtmlFileLogWriter/HtmlFileLogWriter.lvclass"/>
			<Item Name="HumanMachineInterface.lvclass" Type="LVClass" URL="../../../labqt/main/HumanMachineInterface/HumanMachineInterface/HumanMachineInterface.lvclass"/>
			<Item Name="IncomingUserEvent.ctl" Type="VI" URL="../../../labqt/main/GUI/SubPanelUtilities/IncomingUserEvent.ctl"/>
			<Item Name="InitData.ctl" Type="VI" URL="../../../labqt/main/ControlSystem/Regulator/XControl/RegulatorConfiguration/InitData.ctl"/>
			<Item Name="InputStream.lvlib" Type="Library" URL="../../../labqt/main/IOStreams/InputStream/InputStream.lvlib"/>
			<Item Name="InputStreamsToFileOutputStreamHandler.lvclass" Type="LVClass" URL="../../../labqt/main/IOStreams/InputStreamsToFileOutputStreamHandler/InputStreamsToFileOutputStreamHandler.lvclass"/>
			<Item Name="IO.lvclass" Type="LVClass" URL="../../../labqt/main/IOStreams/IO/IO.lvclass"/>
			<Item Name="IoHandler.lvclass" Type="LVClass" URL="../../../labqt/main/IOStreams/IOHandler/IoHandler.lvclass"/>
			<Item Name="JumpCluster.ctl" Type="VI" URL="../../../labqt/main/Sequence/SequenceInputRunner/JumpCluster.ctl"/>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="LeadLag.vi" Type="VI" URL="../../../labqt/main/ControlSystem/Concrete/PIDRegulator/LeadLag.vi"/>
			<Item Name="LinearCalibration.lvclass" Type="LVClass" URL="../../../labqt/main/Calibration/Concrete/LinearCalibration/LinearCalibration.lvclass"/>
			<Item Name="ListFilesWithFileTypes.vi" Type="VI" URL="../../../labqt/main/Utilities/Path/ListFilesWithFileTypes.vi"/>
			<Item Name="ListLogWriter.lvclass" Type="LVClass" URL="../../../labqt/main/Logging/ListLogWriter/ListLogWriter.lvclass"/>
			<Item Name="LocalProcessHMI.lvclass" Type="LVClass" URL="../../../labqt/main/HumanMachineInterface/LocalProcessHMI/LocalProcessHMI.lvclass"/>
			<Item Name="Logger.lvclass" Type="LVClass" URL="../../../labqt/main/Logging/Logger/Logger.lvclass"/>
			<Item Name="LoggerCodec.lvclass" Type="LVClass" URL="../../../labqt/main/Codecs/LoggerCodec/LoggerCodec.lvclass"/>
			<Item Name="LogRapi.lvclass" Type="LVClass" URL="../../../labqt/main/RemoteProcedureCall/RemoteApis/LogRapi/LogRapi.lvclass"/>
			<Item Name="LogWriter.lvclass" Type="LVClass" URL="../../../labqt/main/Logging/LogWriter/LogWriter.lvclass"/>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="MemoryReader.lvclass" Type="LVClass" URL="../../../labqt/main/IOStreams/MemoryReader/MemoryReader.lvclass"/>
			<Item Name="MemoryStream.lvclass" Type="LVClass" URL="../../../labqt/main/IOStreams/MemoryStream/MemoryStream.lvclass"/>
			<Item Name="MemoryWriter.lvclass" Type="LVClass" URL="../../../labqt/main/IOStreams/MemoryWriter/MemoryWriter.lvclass"/>
			<Item Name="MinMaxAlarm.lvclass" Type="LVClass" URL="../../../labqt/main/Alarm/Concrete/MinMaxAlarm/MinMaxAlarm.lvclass"/>
			<Item Name="MinMaxAlarm.xctl" Type="XControl" URL="../../../labqt/main/Alarm/Concrete/MinMaxAlarm/XControl/MinMaxAlarm/MinMaxAlarm.xctl"/>
			<Item Name="MinMaxObjectAttributes.ctl" Type="VI" URL="../../../labqt/main/Alarm/Concrete/MinMaxAlarm/MinMaxObjectAttributes.ctl"/>
			<Item Name="nitaglv.dll" Type="Document" URL="nitaglv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="NormalizeNrOfSamplesToMax.vi" Type="VI" URL="../../../labqt/main/Utilities/Waveform/NormalizeNrOfSamplesToMax.vi"/>
			<Item Name="OutgoingUserEvent.ctl" Type="VI" URL="../../../labqt/main/GUI/SubPanelUtilities/OutgoingUserEvent.ctl"/>
			<Item Name="OutputBufferedDeviceReader.lvclass" Type="LVClass" URL="../../../labqt/main/IOStreams/OutputBufferedDeviceReader/OutputBufferedDeviceReader.lvclass"/>
			<Item Name="OutputStream.lvclass" Type="LVClass" URL="../../../labqt/main/IOStreams/OutputStream/OutputStream.lvclass"/>
			<Item Name="PIDRegulator.lvclass" Type="LVClass" URL="../../../labqt/main/ControlSystem/Concrete/PIDRegulator/PIDRegulator.lvclass"/>
			<Item Name="PidRegulator.xctl" Type="XControl" URL="../../../labqt/main/ControlSystem/Concrete/PIDRegulator/XControl/PidRegulator.xctl"/>
			<Item Name="PidRegulatorObjectAttributes.ctl" Type="VI" URL="../../../labqt/main/ControlSystem/Concrete/PIDRegulator/PidRegulatorObjectAttributes.ctl"/>
			<Item Name="ProcessApplication.lvclass" Type="LVClass" URL="../../../labqt/main/Application/ProcessApplication/ProcessApplication.lvclass"/>
			<Item Name="PropSaver.lvclass" Type="LVClass" URL="../../../labqt/main/Utilities/Properties/PropSaver/PropSaver.lvclass"/>
			<Item Name="Regulator.lvclass" Type="LVClass" URL="../../../labqt/main/ControlSystem/Regulator/Regulator.lvclass"/>
			<Item Name="Regulator.xctl" Type="XControl" URL="../../../labqt/main/ControlSystem/Regulator/XControl/Regulator/Regulator.xctl"/>
			<Item Name="RegulatorConfiguration.xctl" Type="XControl" URL="../../../labqt/main/ControlSystem/Regulator/XControl/RegulatorConfiguration/RegulatorConfiguration.xctl"/>
			<Item Name="RegulatorLister.xctl" Type="XControl" URL="../../../labqt/main/ControlSystem/Regulator/XControl/RegulatorLister/RegulatorLister.xctl"/>
			<Item Name="RegulatorObjectAttributes.ctl" Type="VI" URL="../../../labqt/main/ControlSystem/Regulator/RegulatorObjectAttributes.ctl"/>
			<Item Name="RegulatorStatus.ctl" Type="VI" URL="../../../labqt/main/ControlSystem/Regulator/RegulatorStatus.ctl"/>
			<Item Name="RemoteProcessHMI.lvclass" Type="LVClass" URL="../../../labqt/main/HumanMachineInterface/RemoteProcessHMI/RemoteProcessHMI.lvclass"/>
			<Item Name="RingBuffer.lvlib" Type="Library" URL="../../../labqt/main/Buffer/RingBuffer/RingBuffer.lvlib"/>
			<Item Name="RpcClient.lvclass" Type="LVClass" URL="../../../labqt/main/RemoteProcedureCall/RpcClient/RpcClient.lvclass"/>
			<Item Name="Sequence.xctl" Type="XControl" URL="../../../labqt/main/Sequence/SequenceInputRunner/XControl/Sequence.xctl"/>
			<Item Name="SequenceCodec.lvclass" Type="LVClass" URL="../../../labqt/main/Codecs/SequenceCodec/SequenceCodec.lvclass"/>
			<Item Name="SequenceHandler.lvclass" Type="LVClass" URL="../../../labqt/main/Sequence/SequenceHandler/SequenceHandler.lvclass"/>
			<Item Name="SequenceInputRunner.lvclass" Type="LVClass" URL="../../../labqt/main/Sequence/SequenceInputRunner/SequenceInputRunner.lvclass"/>
			<Item Name="SequenceInputRunnerObjectAttributes.ctl" Type="VI" URL="../../../labqt/main/Sequence/SequenceInputRunner/SequenceInputRunnerObjectAttributes.ctl"/>
			<Item Name="SequenceLister.xctl" Type="XControl" URL="../../../labqt/main/Sequence/SequenceHandler/SequenceLister/SequenceLister.xctl"/>
			<Item Name="SequenceRapi.lvclass" Type="LVClass" URL="../../../labqt/main/RemoteProcedureCall/RemoteApis/SequenceRapi/SequenceRapi.lvclass"/>
			<Item Name="SequenceSeriesType.ctl" Type="VI" URL="../../../labqt/main/Sequence/SequenceInputRunner/SequenceSeriesType.ctl"/>
			<Item Name="SetBusyDialog.vi" Type="VI" URL="../../../labqt/main/Utilities/Dialog/BusyDialog/SetBusyDialog.vi"/>
			<Item Name="SetWaveformChannelNames.vi" Type="VI" URL="../../../labqt/main/Utilities/Waveform/SetWaveformChannelNames.vi"/>
			<Item Name="SimpleFeedbackControlSequence.lvclass" Type="LVClass" URL="../../../labqt/main/Sequence/Concrete/SimpleFeedbackControlSequence/SimpleFeedbackControlSequence.lvclass"/>
			<Item Name="SocketReader.lvclass" Type="LVClass" URL="../../../labqt/main/IOStreams/SocketReader/SocketReader.lvclass"/>
			<Item Name="SocketStream.lvclass" Type="LVClass" URL="../../../labqt/main/IOStreams/SocketStream/SocketStream.lvclass"/>
			<Item Name="SocketWriter.lvclass" Type="LVClass" URL="../../../labqt/main/IOStreams/SocketWriter/SocketWriter.lvclass"/>
			<Item Name="Stream.lvclass" Type="LVClass" URL="../../../labqt/main/IOStreams/Stream/Stream.lvclass"/>
			<Item Name="StringToDouble.vi" Type="VI" URL="../../../labqt/main/Utilities/Conversion/StringToDouble.vi"/>
			<Item Name="SubPanelIncomingData.ctl" Type="VI" URL="../../../labqt/main/GUI/SubPanelUtilities/SubPanelIncomingData.ctl"/>
			<Item Name="SubPanelInitiate.vi" Type="VI" URL="../../../labqt/main/GUI/SubPanelUtilities/SubPanelInitiate.vi"/>
			<Item Name="SubPanelOutgoingData.ctl" Type="VI" URL="../../../labqt/main/GUI/SubPanelUtilities/SubPanelOutgoingData.ctl"/>
			<Item Name="TCP_NODELAY.vi" Type="VI" URL="../../../labqt/main/IOStreams/SupportVIs/TCP_NODELAY.vi"/>
			<Item Name="TdmsFileWriter.lvclass" Type="LVClass" URL="../../../labqt/main/IOStreams/TdmsFileWriter/TdmsFileWriter.lvclass"/>
			<Item Name="TextFileLogWriter.lvclass" Type="LVClass" URL="../../../labqt/main/Logging/TextFileLogWriter/TextFileLogWriter.lvclass"/>
			<Item Name="Threading.lvclass" Type="LVClass" URL="../../../labqt/main/Execution/Threading/Threading.lvclass"/>
			<Item Name="UnbufferedDeviceReader.lvclass" Type="LVClass" URL="../../../labqt/main/IOStreams/UnbufferedDeviceReader/UnbufferedDeviceReader.lvclass"/>
			<Item Name="UnbufferedDeviceWriter.lvclass" Type="LVClass" URL="../../../labqt/main/IOStreams/UnbufferedDeviceWriter/UnbufferedDeviceWriter.lvclass"/>
			<Item Name="url_decode.vi" Type="VI" URL="../../../labqt/main/Utilities/String/url_decode.vi"/>
			<Item Name="UserDefinedErrorReasons.lvclass" Type="LVClass" URL="../../../labqt/main/Logging/UserDefinedErrorReasons/UserDefinedErrorReasons.lvclass"/>
			<Item Name="UserEventType.ctl" Type="VI" URL="../../../labqt/main/GUI/SubPanelUtilities/UserEventType.ctl"/>
			<Item Name="version.dll" Type="Document" URL="version.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="WaitForOpen.vi" Type="VI" URL="../../../labqt/main/GUI/SubPanelUtilities/WaitForOpen.vi"/>
			<Item Name="wsock32.dll" Type="Document" URL="wsock32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="ProcessRemoteClient" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{A4E9A594-7F94-40CE-987E-72B71696D76C}</Property>
				<Property Name="App_INI_GUID" Type="Str">{A3C5700D-90D4-44FE-891A-5082106B80E9}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{07F4208E-DF33-4148-B11F-4EEBC61CA493}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">ProcessRemoteClient</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeTypedefs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/ProcessRemoteClient</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{00D31E8B-6841-4330-B944-D7C3CF457F18}</Property>
				<Property Name="Bld_version.build" Type="Int">9</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">ProcessRemoteClient.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/ProcessRemoteClient/ProcessRemoteClient.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/ProcessRemoteClient/data</Property>
				<Property Name="Destination[2].destName" Type="Str">HTML</Property>
				<Property Name="Destination[2].path" Type="Path">../builds/ProcessRemoteClient/data/HTML</Property>
				<Property Name="DestinationCount" Type="Int">3</Property>
				<Property Name="Exe_Vardep[0].LibDeploy" Type="Bool">true</Property>
				<Property Name="Exe_Vardep[0].LibItemID" Type="Ref">/My Computer/InputVariables/AnalogInInstr.lvlib</Property>
				<Property Name="Exe_Vardep[1].LibDeploy" Type="Bool">true</Property>
				<Property Name="Exe_Vardep[1].LibItemID" Type="Ref">/My Computer/VariablesSequence/Criterias.lvlib</Property>
				<Property Name="Exe_Vardep[10].LibDeploy" Type="Bool">true</Property>
				<Property Name="Exe_Vardep[10].LibItemID" Type="Ref">/My Computer/VariablesSequence/UserOutputs.lvlib</Property>
				<Property Name="Exe_Vardep[11].LibDeploy" Type="Bool">true</Property>
				<Property Name="Exe_Vardep[11].LibItemID" Type="Ref">/My Computer/OutputVariables/DigitalOutInstr.lvlib</Property>
				<Property Name="Exe_Vardep[2].LibDeploy" Type="Bool">true</Property>
				<Property Name="Exe_Vardep[2].LibItemID" Type="Ref">/My Computer/VariablesSequence/RegulatorParameters.lvlib</Property>
				<Property Name="Exe_Vardep[3].LibDeploy" Type="Bool">true</Property>
				<Property Name="Exe_Vardep[3].LibItemID" Type="Ref">/My Computer/VariablesSequence/SequenceData.lvlib</Property>
				<Property Name="Exe_Vardep[4].LibDeploy" Type="Bool">true</Property>
				<Property Name="Exe_Vardep[4].LibItemID" Type="Ref">/My Computer/VariablesSequence/SequenceLogic.lvlib</Property>
				<Property Name="Exe_Vardep[5].LibDeploy" Type="Bool">true</Property>
				<Property Name="Exe_Vardep[5].LibItemID" Type="Ref">/My Computer/VariablesSequence/SupportVariables.lvlib</Property>
				<Property Name="Exe_Vardep[6].LibDeploy" Type="Bool">true</Property>
				<Property Name="Exe_Vardep[6].LibItemID" Type="Ref">/My Computer/InputVariables/AnalogInDuts.lvlib</Property>
				<Property Name="Exe_Vardep[7].LibDeploy" Type="Bool">true</Property>
				<Property Name="Exe_Vardep[7].LibItemID" Type="Ref">/My Computer/InputVariables/AnalogInSbRIO.lvlib</Property>
				<Property Name="Exe_Vardep[8].LibDeploy" Type="Bool">true</Property>
				<Property Name="Exe_Vardep[8].LibItemID" Type="Ref">/My Computer/OutputVariables/AnalogOutInstr.lvlib</Property>
				<Property Name="Exe_Vardep[9].LibDeploy" Type="Bool">true</Property>
				<Property Name="Exe_Vardep[9].LibItemID" Type="Ref">/My Computer/VariablesSequence/Setpoints.lvlib</Property>
				<Property Name="Exe_VardepDeployAtStartup" Type="Bool">true</Property>
				<Property Name="Exe_VardepHideDeployDlg" Type="Bool">true</Property>
				<Property Name="Exe_VardepLibItemCount" Type="Int">12</Property>
				<Property Name="Exe_VardepUndeployOnExit" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{C815D4EC-4A8E-44CD-A12A-9542597668CA}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/InputVariables/AnalogInInstr.lvlib</Property>
				<Property Name="Source[1].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[1].type" Type="Str">Library</Property>
				<Property Name="Source[10].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[10].itemID" Type="Ref">/My Computer/VariablesSequence/UserOutputs.lvlib</Property>
				<Property Name="Source[10].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[10].type" Type="Str">Library</Property>
				<Property Name="Source[11].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[11].itemID" Type="Ref">/My Computer/ProjectHMI.lvclass/Main.vi</Property>
				<Property Name="Source[11].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[11].type" Type="Str">VI</Property>
				<Property Name="Source[12].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[12].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[12].destinationIndex" Type="Int">2</Property>
				<Property Name="Source[12].itemID" Type="Ref">/My Computer/ProjectHMI.lvclass/SupportFiles/HTML</Property>
				<Property Name="Source[12].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[12].type" Type="Str">Container</Property>
				<Property Name="Source[13].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[13].destinationIndex" Type="Int">2</Property>
				<Property Name="Source[13].itemID" Type="Ref">/My Computer/ProjectHMI.lvclass/SupportFiles</Property>
				<Property Name="Source[13].type" Type="Str">Container</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/OutputVariables/AnalogOutInstr.lvlib</Property>
				<Property Name="Source[2].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[2].type" Type="Str">Library</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/OutputVariables/DigitalOutInstr.lvlib</Property>
				<Property Name="Source[3].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[3].type" Type="Str">Library</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/VariablesSequence/Criterias.lvlib</Property>
				<Property Name="Source[4].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[4].type" Type="Str">Library</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/VariablesSequence/RegulatorParameters.lvlib</Property>
				<Property Name="Source[5].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[5].type" Type="Str">Library</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/VariablesSequence/SequenceData.lvlib</Property>
				<Property Name="Source[6].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[6].type" Type="Str">Library</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/VariablesSequence/SequenceLogic.lvlib</Property>
				<Property Name="Source[7].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[7].type" Type="Str">Library</Property>
				<Property Name="Source[8].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/VariablesSequence/Setpoints.lvlib</Property>
				<Property Name="Source[8].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[8].type" Type="Str">Library</Property>
				<Property Name="Source[9].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[9].itemID" Type="Ref">/My Computer/VariablesSequence/SupportVariables.lvlib</Property>
				<Property Name="Source[9].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[9].type" Type="Str">Library</Property>
				<Property Name="SourceCount" Type="Int">14</Property>
				<Property Name="TgtF_companyName" Type="Str">DVel AB</Property>
				<Property Name="TgtF_fileDescription" Type="Str">ProcessRemoteClient</Property>
				<Property Name="TgtF_internalName" Type="Str">ProcessRemoteClient</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright 2013 DVel AB</Property>
				<Property Name="TgtF_productName" Type="Str">ProcessRemoteClient</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{D5FF7B5F-B74F-4663-86BF-72E045893A25}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">ProcessRemoteClient.exe</Property>
			</Item>
			<Item Name="ProcessRemoteClient_InstallerWithDrivers" Type="Installer">
				<Property Name="Destination[0].name" Type="Str">PressureTestRig</Property>
				<Property Name="Destination[0].parent" Type="Str">{3912416A-D2E5-411B-AFEE-B63654D690C0}</Property>
				<Property Name="Destination[0].tag" Type="Str">{12ADBC76-9F96-4E37-8B95-6CDE5109A541}</Property>
				<Property Name="Destination[0].type" Type="Str">userFolder</Property>
				<Property Name="DestinationCount" Type="Int">1</Property>
				<Property Name="DistPart[0].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[0].productID" Type="Str">{C34C4274-13B4-47B7-BC33-1D8996632A27}</Property>
				<Property Name="DistPart[0].productName" Type="Str">NI LabVIEW Runtime 2014 SP1 f11</Property>
				<Property Name="DistPart[0].upgradeCode" Type="Str">{4722F14B-8434-468D-840D-2B0CD8CBD5EA}</Property>
				<Property Name="DistPart[1].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[1].productID" Type="Str">{0CE88222-B308-4398-BBC5-12889B169A35}</Property>
				<Property Name="DistPart[1].productName" Type="Str">NI Measurement &amp; Automation Explorer 5.4</Property>
				<Property Name="DistPart[1].upgradeCode" Type="Str">{AE940F24-CC0E-4148-9A96-10FB04D9796D}</Property>
				<Property Name="DistPart[2].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[2].productID" Type="Str">{9CD98CEE-3271-4F0E-9C06-75A1EE9E103F}</Property>
				<Property Name="DistPart[2].productName" Type="Str">NI TDM Excel Add-In for Microsoft Excel</Property>
				<Property Name="DistPart[2].upgradeCode" Type="Str">{6D2EBDAF-6CCD-44F3-B767-4DF9E0F2037B}</Property>
				<Property Name="DistPart[3].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[3].productID" Type="Str">{EA9650DD-039A-4D72-8967-0FEEFDFB36B0}</Property>
				<Property Name="DistPart[3].productName" Type="Str">NI Variable Engine 2.6.0</Property>
				<Property Name="DistPart[3].upgradeCode" Type="Str">{EB7A3C81-1C0F-4495-8CE5-0A427E4E6285}</Property>
				<Property Name="DistPart[4].flavorID" Type="Str">DriverOnly</Property>
				<Property Name="DistPart[4].productID" Type="Str">{05544276-41A0-4FED-ACE5-15A984A4707A}</Property>
				<Property Name="DistPart[4].productName" Type="Str">NI-RIO 4.1</Property>
				<Property Name="DistPart[4].upgradeCode" Type="Str">{DC26B58E-A453-4A11-BEDB-300B0F2DDDBF}</Property>
				<Property Name="DistPart[5].flavorID" Type="Str">_deployment_</Property>
				<Property Name="DistPart[5].productID" Type="Str">{B4F17552-FEA2-40BC-82CA-4F1DF61FF1A8}</Property>
				<Property Name="DistPart[5].productName" Type="Str">NI-VISA Runtime 5.2</Property>
				<Property Name="DistPart[5].upgradeCode" Type="Str">{8627993A-3F66-483C-A562-0D3BA3F267B1}</Property>
				<Property Name="DistPartCount" Type="Int">6</Property>
				<Property Name="INST_author" Type="Str">Symbio</Property>
				<Property Name="INST_autoIncrement" Type="Bool">true</Property>
				<Property Name="INST_buildLocation" Type="Path">../builds/ProcessRemoteClient_InstallerWithDrivers</Property>
				<Property Name="INST_buildLocation.type" Type="Str">relativeToCommon</Property>
				<Property Name="INST_buildSpecName" Type="Str">ProcessRemoteClient_InstallerWithDrivers</Property>
				<Property Name="INST_defaultDir" Type="Str">{12ADBC76-9F96-4E37-8B95-6CDE5109A541}</Property>
				<Property Name="INST_productName" Type="Str">ProcessRemoteClient</Property>
				<Property Name="INST_productVersion" Type="Str">1.0.3</Property>
				<Property Name="InstSpecBitness" Type="Str">32-bit</Property>
				<Property Name="InstSpecVersion" Type="Str">14018027</Property>
				<Property Name="MSI_arpCompany" Type="Str">DVel AB</Property>
				<Property Name="MSI_arpContact" Type="Str">Per Hedlund, Roger Isaksson</Property>
				<Property Name="MSI_arpPhone" Type="Str">0733856923</Property>
				<Property Name="MSI_arpURL" Type="Str">http://www.dvel.se/</Property>
				<Property Name="MSI_distID" Type="Str">{2E3B522D-6FDB-4857-8C13-BA39021297D8}</Property>
				<Property Name="MSI_osCheck" Type="Int">0</Property>
				<Property Name="MSI_upgradeCode" Type="Str">{9CF37173-36D2-458C-8823-F271509679DF}</Property>
				<Property Name="MSI_windowMessage" Type="Str">This installation also includes NI device drivers.</Property>
				<Property Name="MSI_windowTitle" Type="Str">Pressure Test Rig Control</Property>
				<Property Name="RegDest[0].dirName" Type="Str">Software</Property>
				<Property Name="RegDest[0].dirTag" Type="Str">{DDFAFC8B-E728-4AC8-96DE-B920EBB97A86}</Property>
				<Property Name="RegDest[0].parentTag" Type="Str">2</Property>
				<Property Name="RegDestCount" Type="Int">1</Property>
				<Property Name="Source[0].dest" Type="Str">{12ADBC76-9F96-4E37-8B95-6CDE5109A541}</Property>
				<Property Name="Source[0].File[0].dest" Type="Str">{12ADBC76-9F96-4E37-8B95-6CDE5109A541}</Property>
				<Property Name="Source[0].File[0].name" Type="Str">ProcessRemoteClient.exe</Property>
				<Property Name="Source[0].File[0].Shortcut[0].destIndex" Type="Int">0</Property>
				<Property Name="Source[0].File[0].Shortcut[0].name" Type="Str">Application</Property>
				<Property Name="Source[0].File[0].Shortcut[0].subDir" Type="Str">PressureTestRig</Property>
				<Property Name="Source[0].File[0].ShortcutCount" Type="Int">1</Property>
				<Property Name="Source[0].File[0].tag" Type="Str">{D5FF7B5F-B74F-4663-86BF-72E045893A25}</Property>
				<Property Name="Source[0].FileCount" Type="Int">1</Property>
				<Property Name="Source[0].name" Type="Str">ProcessRemoteClient</Property>
				<Property Name="Source[0].tag" Type="Ref">/My Computer/Build Specifications/ProcessRemoteClient</Property>
				<Property Name="Source[0].type" Type="Str">EXE</Property>
				<Property Name="SourceCount" Type="Int">1</Property>
			</Item>
			<Item Name="ProcessRemoteClient_Installer" Type="Installer">
				<Property Name="Destination[0].name" Type="Str">PressureTestRig</Property>
				<Property Name="Destination[0].parent" Type="Str">{3912416A-D2E5-411B-AFEE-B63654D690C0}</Property>
				<Property Name="Destination[0].tag" Type="Str">{12ADBC76-9F96-4E37-8B95-6CDE5109A541}</Property>
				<Property Name="Destination[0].type" Type="Str">userFolder</Property>
				<Property Name="DestinationCount" Type="Int">1</Property>
				<Property Name="INST_author" Type="Str">Symbio</Property>
				<Property Name="INST_autoIncrement" Type="Bool">true</Property>
				<Property Name="INST_buildLocation" Type="Path">../builds/ProcessRemoteClient_Installer</Property>
				<Property Name="INST_buildLocation.type" Type="Str">relativeToCommon</Property>
				<Property Name="INST_buildSpecName" Type="Str">ProcessRemoteClient_Installer</Property>
				<Property Name="INST_defaultDir" Type="Str">{12ADBC76-9F96-4E37-8B95-6CDE5109A541}</Property>
				<Property Name="INST_productName" Type="Str">ProcessRemoteClient</Property>
				<Property Name="INST_productVersion" Type="Str">1.0.5</Property>
				<Property Name="InstSpecBitness" Type="Str">32-bit</Property>
				<Property Name="InstSpecVersion" Type="Str">14018027</Property>
				<Property Name="MSI_arpCompany" Type="Str">DVel AB</Property>
				<Property Name="MSI_arpContact" Type="Str">Per Hedlund</Property>
				<Property Name="MSI_arpPhone" Type="Str">0733856923</Property>
				<Property Name="MSI_arpURL" Type="Str">http://www.dvel.se/</Property>
				<Property Name="MSI_distID" Type="Str">{041E3D63-402B-4824-8953-AAEB8066D51A}</Property>
				<Property Name="MSI_osCheck" Type="Int">0</Property>
				<Property Name="MSI_upgradeCode" Type="Str">{7975E49D-2FE4-4355-A73C-8411D8B9A95E}</Property>
				<Property Name="MSI_windowMessage" Type="Str">This installation does not contain drivers, only updates for the actual application.</Property>
				<Property Name="MSI_windowTitle" Type="Str">Pressure Test Rig Control</Property>
				<Property Name="RegDest[0].dirName" Type="Str">Software</Property>
				<Property Name="RegDest[0].dirTag" Type="Str">{DDFAFC8B-E728-4AC8-96DE-B920EBB97A86}</Property>
				<Property Name="RegDest[0].parentTag" Type="Str">2</Property>
				<Property Name="RegDestCount" Type="Int">1</Property>
				<Property Name="Source[0].dest" Type="Str">{12ADBC76-9F96-4E37-8B95-6CDE5109A541}</Property>
				<Property Name="Source[0].File[0].dest" Type="Str">{12ADBC76-9F96-4E37-8B95-6CDE5109A541}</Property>
				<Property Name="Source[0].File[0].name" Type="Str">ProcessRemoteClient.exe</Property>
				<Property Name="Source[0].File[0].Shortcut[0].destIndex" Type="Int">0</Property>
				<Property Name="Source[0].File[0].Shortcut[0].name" Type="Str">Application</Property>
				<Property Name="Source[0].File[0].Shortcut[0].subDir" Type="Str">PressureTestRig</Property>
				<Property Name="Source[0].File[0].ShortcutCount" Type="Int">1</Property>
				<Property Name="Source[0].File[0].tag" Type="Str">{D5FF7B5F-B74F-4663-86BF-72E045893A25}</Property>
				<Property Name="Source[0].FileCount" Type="Int">1</Property>
				<Property Name="Source[0].name" Type="Str">ProcessRemoteClient</Property>
				<Property Name="Source[0].tag" Type="Ref">/My Computer/Build Specifications/ProcessRemoteClient</Property>
				<Property Name="Source[0].type" Type="Str">EXE</Property>
				<Property Name="SourceCount" Type="Int">1</Property>
			</Item>
		</Item>
	</Item>
</Project>
